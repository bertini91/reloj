function obtenerHora(){
    let fecha = new Date();

    let diaSemana = document.getElementById("diaSemana");
    let diaMes = document.getElementById("diaNumero");
    let mes = document.getElementById("mes"),
        anio = document.getElementById("anio"),
        horas = document.getElementById("horas"),
        minutos = document.getElementById("minutos"),
        segundos = document.getElementById("segundos"),
        amPm = document.getElementById("amPm");
    var meses = ["Enero","Febrero","Marzo","Abril","Mayo","Junio",
        "Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
    diaSemana.innerHTML = obtenerDia(fecha.getDay());
    diaMes.innerHTML = fecha.getDate();
    mes.innerHTML = meses[fecha.getMonth()];
    anio.innerHTML = fecha.getFullYear();

    if((fecha.getHours()-12)<10){
        horas.innerHTML = "0"+(fecha.getHours()-12);
    }else{
        horas.innerHTML = (fecha.getHours()-12);
    }

    if(fecha.getMinutes()<10){
        minutos.innerHTML = "0"+ fecha.getMinutes();
    }else{
        minutos.innerHTML = fecha.getMinutes();
    }
    if(fecha.getSeconds() < 10){
        segundos.innerHTML = "0"+ fecha.getSeconds();
    }else{
        segundos.innerHTML = fecha.getSeconds();
    }
 
    if(fecha.getHours() >= 12){
        amPm.innerHTML = "PM";
    }else{
        amPm.innerHTML = "AM";
    }
}

function obtenerDia(dia){
    switch(true){
        case dia == 0:
            return "Domingo";
        case dia == 1:
            return "Lunes";
        case dia == 2:
            return "Martes";
        case dia == 3:
            return "Miercoles";
        case dia == 4:
            return "Jueves";
        case dia == 5:
            return "Viernes";
        case dia == 6:
            return "Sabado";
    }
}
let tiempo = window.setInterval(obtenerHora, 1000);
obtenerHora();